// (function() {
//   $(".services-slider").slick({
//     slidesToShow: 4,
//     slidesToScroll: 1,
//     arrow: true,
//     centerMode: true,
//     focusOnSelect: true
//   });
// })();

// let sliderTitle = [
//   "Транспортные услуги",
//   "Водительские услуги услуги",
//   "Еда услуги",
//   "Велосипедные услуги",
//   "Горные услуги",
//   "Кымызолечение",
//   "Разные услуги",
//   "Хорошие услуги",
//   "Гууд услуги",
//   "Смалл услуги"
// ];

// let sliderImage = [
//   "../../images/photo/transport_services.png",
//   "../../images/photo/two.svg",
//   "../../images/photo/three.svg",
//   "../../images/photo/four.svg",
//   "../../images/photo/five.svg",
//   "../../images/photo/six.svg",
//   "../../images/photo/seven.svg",
//   "../../images/photo/seven.svg",
//   "../../images/photo/seven.svg",
//   "../../images/photo/seven.svg"
// ];

// $(".services-slider__card").click(function() {
//   switch (this.id) {
//     case "one":
//       $(".services-text__title").html(sliderTitle[0]);
//       document.querySelector('.prewi-image').src = sliderImage[0]
//       break;
//     case "two":
//       $(".services-text__title").html(sliderTitle[1]);
//       document.querySelector('.prewi-image').src = sliderImage[1]
//       break;
//     case "three":
//       $(".services-text__title").html(sliderTitle[2]);
//       document.querySelector('.prewi-image').src = sliderImage[2]
//       break;
//     case "four":
//       $(".services-text__title").html(sliderTitle[3]);
//       document.querySelector('.prewi-image').src = sliderImage[3]
//       break;
//     case "five":
//       $(".services-text__title").html(sliderTitle[4]);
//       document.querySelector('.prewi-image').src = sliderImage[4]
//       break;
//     case "six":
//       $(".services-text__title").html(sliderTitle[5]);
//       document.querySelector('.prewi-image').src = sliderImage[5]
//       break;
//     case "seven":
//       $(".services-text__title").html(sliderTitle[6]);
//       document.querySelector('.prewi-image').src = sliderImage[6]
//       break;
//     case "eight":
//       $(".services-text__title").html(sliderTitle[7]);
//       document.querySelector('.prewi-image').src = sliderImage[7]
//       break;
//     case "nine":
//       $(".services-text__title").html(sliderTitle[8]);
//       document.querySelector('.prewi-image').src = sliderImage[8]
//       break;
//     case "ten":
//       $(".services-text__title").html(sliderTitle[9]);
//       document.querySelector('.prewi-image').src = sliderImage[9]
//       break;
//     default:
//       break;
//   }
// });

$(".slider-for").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".slider-nav"
});
$(".slider-nav").slick({
  slidesToShow: 4.5,
  slidesToScroll: 1,
  // infinite: false,
  asNavFor: ".slider-for",
  focusOnSelect: true
});

$(".slider-review").slick({
  infinite: true,
  slidesToShow: 4.5,
  slidesToScroll: 1
});

$(document).ready(function() {
  $("a").click(function() {
    var elementClick = $(this).attr("href");
    var destination = $(elementClick).offset().top;
    $("html").animate({ scrollTop: destination }, 1100);
    return false;
  });
});
