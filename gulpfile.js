var gulp = require("gulp"),
  sass = require("gulp-sass"),
  browserSync = require("browser-sync"),
  concatCss = require("gulp-concat-css"),
  cssmin = require("gulp-cssmin"),
  rename = require("gulp-rename");

gulp.task("sass", function() {
  return gulp
    .src("app/sass/**/*.scss")
    .pipe(sass())
    .pipe(concatCss("main.css"))
    .pipe(gulp.dest("app/styles/"))
    .pipe(cssmin())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("app/styles/"))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("browser-sync", function() {
  browserSync({
    server: {
      baseDir: "app"
    },
    notify: false
  });
});

gulp.task("watch", function() {
  gulp.watch("app/sass/**/*.scss", gulp.series("sass")); // Наблюдение за sass файлами
  gulp.watch("app/*.html").on("change", browserSync.reload);
  gulp.watch("app/scripts/script.js").on("change", browserSync.reload);
  gulp.watch("app/styles/**/*.css").on("change", browserSync.stream); // Обновляем CSS на странице при изменении

  // Наблюдение за другими типами файлов
});

gulp.task("start", gulp.parallel("watch", "sass", "browser-sync"));
